<?php

namespace DeinonychusCowboy\TimeTrackerBundle\Lib;

class Helper
{
	// theme options
	const PASTEL_BRIGHT = 255;
	const PASTEL_NORMAL = 223;
	const LIGHT_BRIGHT  = 159;
	const LIGHT_NORMAL  = 127;
	const MAIN_BRIGHT   = 95;
	const MAIN_NORMAL   = 0;
	private static $pastel   = self::PASTEL_NORMAL;
	private static $light    = self::LIGHT_BRIGHT;
	private static $main     = self::MAIN_BRIGHT;
	private static $inverted = false;

	public static function generateId()
	{
		return base_convert(hexdec(sha1(uniqid("",true))),10,32);
	}

	/**
	 * @return array
	 */
	public static function getColorInternal($hash,$basis)
	{
		$col = array(0 => 0,1 => 0,2 => 0);
		for($i = 0;$i < strlen($hash);$i++)
		{
			$col[$i % 3] += hexdec($hash[$i]);
		}
		// modulus step
		foreach(array_keys($col) as $key)
		{
			$col[$key] %= 255;
		}
		// maxify step
		$max = 0;
		foreach(array_keys($col) as $key)
		{
			$max = $max >= $col[$key]
				? $max
				: $col[$key];
		}
		foreach(array_keys($col) as $key)
		{
			$col[$key] /= $max + 0.0;
			$col[$key] *= 255;
		}
		// roundoff step
		$basis = self::$inverted
			? (255 - $basis)
			: $basis;
		$red   = (int)($col[0] / 255.0 * $basis);
		$green = (int)($col[1] / 255.0 * $basis);
		$blue  = (int)($col[2] / 255.0 * $basis);

		return array($red,$green,$blue);
	}

	public static function getMainColor($hash)
	{
		return self::getColorInternal($hash,self::$main);
	}

	public static function getLightColor($hash)
	{
		return self::getColorInternal($hash,self::$light);
	}

	public static function getPastelColor($hash)
	{
		return self::getColorInternal($hash,self::$pastel);
	}

	public static function generateHash($seed)
	{
		return md5($seed);
	}
}
