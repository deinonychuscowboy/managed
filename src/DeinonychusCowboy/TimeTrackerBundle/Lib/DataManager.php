<?php

namespace DeinonychusCowboy\TimeTrackerBundle\Lib;

use DeinonychusCowboy\TimeTrackerBundle\Entity\Interval;
use DeinonychusCowboy\TimeTrackerBundle\Entity\Tag;
use DeinonychusCowboy\TimeTrackerBundle\Entity\Task;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\PersistentCollection;

class DataManager
{
	private static $taskRepository;
	private static $tagRepository;
	private static $intervalRepository;
	private static $tagGroupRepository;
	private static $em;
	private static $datastore = array();

	/**
	 * @return array
	 */
	private static function _closedInternal($closedState,$limit)
	{
		// TODO Limit
		$arr  = self::$em->createQuery(
			"SELECT t.id FROM TimeTrackerBundle:Task t WHERE t.closed IS" . ($closedState
				? " NOT "
				: " ") . "NULL"
		)->getResult();
		$objs = array();
		foreach($arr as $id)
		{
			$objs[] = self::findById($id["id"]);
		}

		return $objs;
	}

	public static function init($doctrine)
	{
		self::$em                 = $doctrine->getManager();
		self::$taskRepository     = $doctrine->getRepository("TimeTrackerBundle:Task");
		self::$tagRepository      = $doctrine->getRepository("TimeTrackerBundle:Tag");
		self::$tagGroupRepository = $doctrine->getRepository("TimeTrackerBundle:TagGroup");
		self::$intervalRepository = $doctrine->getRepository("TimeTrackerBundle:Interval");
	}

	private static function deserialize($id)
	{
		$obj = null;
		switch($id[0])
		{
			case "G":
				$obj = self::$tagRepository->find($id);
				break;
			case "T":
				$obj = self::$taskRepository->find($id);
				break;
			case "I":
				$obj = self::$intervalRepository->find($id);
				break;
			case "R":
				$obj = self::$tagGroupRepository->find($id);
				break;
		}
		if(!$obj)
		{
			throw new \Exception("Invalid ID!");
		}
		self::$datastore[$id] = $obj;
	}

	public static function findById($id)
	{
		if(!array_key_exists($id,self::$datastore))
		{
			self::deserialize($id);
		}

		return self::$datastore[$id];
	}

	public static function store($obj)
	{
		self::$datastore[$obj->getId()] = $obj;
	}

	public static function flush($obj)
	{
		unset(self::$datastore[$obj->getId()]);
	}

	public static function serializeAll()
	{
		foreach(self::$datastore as $id => $obj)
		{
			self::serialize($id);
		}
	}

	public static function serialize($id)
	{
		self::$em->persist(self::$datastore[$id]);
	}

	private static function getAllInternal($type,$limit)
	{
		// TODO Limit
		$arr  = self::$em->createQuery("SELECT o.id FROM TimeTrackerBundle:" . $type . " o")->getResult();
		$objs = array();
		foreach($arr as $id)
		{
			$objs[] = self::findById($id["id"]);
		}

		return $objs;
	}

	public static function getAllTasks($limit = null)
	{
		return self::getAllInternal("Task",$limit);
	}

	public static function getAllTags($limit = null)
	{
		return self::getAllInternal("Tag",$limit);
	}

	public static function getAllTagGroups($limit = null)
	{
		return self::getAllInternal("TagGroup",$limit);
	}

	public static function getAllIntervals($limit = null)
	{
		// TODO Limit
		$arr  = self::$em->createQuery("SELECT o.id FROM TimeTrackerBundle:Interval o ORDER BY o.start DESC")
			->getResult();
		$objs = array();
		foreach($arr as $id)
		{
			$objs[] = self::findById($id["id"]);
		}

		return $objs;
	}

	public static function filterWithTag($objs,$tagid,$taskCallback = null,$limit = null)
	{
		$ret = array();
		foreach($objs as $obj)
		{
			if(in_array(
				$tagid,
				$taskCallback === null
					? $obj->getTagIds()
					: $taskCallback($obj)->getTagIds()
			))
			{
				$ret[] = $obj;
			}
			if(count($ret) === $limit)
			{
				break;
			}
		}

		return $ret;
	}

	public static function filterByCloseDate($tasks,$from,$to,$limit = null)
	{
		uasort(
			$tasks,
			function ($a,$b)
			{
				return $a->getCloseUnixtime() - $b->getCloseUnixtime();
			}
		);
		$ret = array();
		foreach($tasks as $task)
		{
			if($task->getCloseUnixtime() < $from)
			{
				continue;
			}
			elseif($task->getCloseUnixtime() < $to)
			{
				$ret[] = $task;
				if(count($ret) === $limit)
				{
					break;
				}
			}
			else
			{
				break;
			}
		}

		return $ret;
	}

	public static function getAllOpenTasks($limit = null)
	{
		return self::_closedInternal(false,$limit);
	}

	public static function getAllClosedTasks($limit = null)
	{
		return self::_closedInternal(true,$limit);
	}

	public static function commit()
	{
		self::$em->flush();
	}

	public static function newTagCollection()
	{
		return new PersistentCollection(
			self::$em,new ClassMetadata("\\DeinonychusCowboy\\TimeTrackerBundle\\Entity\\Tag"),array()
		);
	}

	public static function newTagGroupCollection()
	{
		return new PersistentCollection(
			self::$em,new ClassMetadata("\\DeinonychusCowboy\\TimeTrackerBundle\\Entity\\TagGroup"),array()
		);
	}

	public static function newTaskCollection()
	{
		return new PersistentCollection(
			self::$em,new ClassMetadata("\\DeinonychusCowboy\\TimeTrackerBundle\\Entity\\Task"),array()
		);
	}

	public static function newIntervalCollection()
	{
		return new PersistentCollection(
			self::$em,new ClassMetadata("\\DeinonychusCowboy\\TimeTrackerBundle\\Entity\\Interval"),array()
		);
	}
}
