function countClosedTasks(data) {
    return {
        chart: {
            type: 'column',
            zoomType: 'x'
        },
        title: {
            text: 'Tag Clearance'
        },
        xAxis: {
            type: 'datetime',
            minRange: 14 * 24 * 3600000 // fourteen days
        },
        yAxis: {
            title: {
                text: 'Velocity'
            },
            labels:{
                enabled:false
            },
            min:0
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            area: {
                stacking: "normal"
            }
        },

        tooltip: {
            valueSuffix: ' closed tasks had this tag'
        },

        series: data
    };
}
function timeClosedTasks(data) {
    return {
        chart: {
            type: 'column',
            zoomType: 'x'
        },
        title: {
            text: 'Time Clearance'
        },
        xAxis: {
            type: 'datetime',
            minRange: 14 * 24 * 3600000 // fourteen days
        },
        yAxis: {
            title: {
                text: 'Allotment'
            },
            labels:{
                enabled:false
            },
            min:0
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            area: {
                stacking: "normal"
            }
        },

        tooltip: {
            valueSuffix: ' seconds were spent on this tag'
        },

        series: data
    };
}
