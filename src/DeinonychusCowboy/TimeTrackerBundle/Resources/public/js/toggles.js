$(function () {
    var transform = function (test, seton, setoff, texton, textoff) {
        return function () {
            var object = $(this);
            $(this).after('<div><div class="toggle-box"><div class="toggle-left"><div class="toggle-on"><div class="toggle-on-inner"><div class="toggle-on-text">' + texton(object) + '</div></div></div><div class="toggle-switch"></div></div><div class="toggle-off"><div class="toggle-off-text">' + textoff(object) + '</div></div></div></div>');
            var toggle = $(this).next().children();
            var handler = function () {
                if (object.attr("disabled") == "disabled") {
                    toggle.addClass("disabled");
                } else {
                    toggle.removeClass("disabled");
                }
                if (!toggle.hasClass("disabled")) {
                    if (test(object)) {
                        setoff(object);
                        toggle.find(".toggle-on").animate({width: 0}, function () {
                            $(this).hide();
                        });
                    } else {
                        seton(object);
                        toggle.find(".toggle-on").show().animate({width: toggle.width() + "px"});
                    }
                }
            };
            toggle.on('click', handler);
            if (test(object)) {
                toggle.find(".toggle-on").css({width: toggle.width() + "px"});
            } else {
                toggle.find(".toggle-on").css({width: 0}).hide();
            }
            if (object.attr("disabled") == "disabled") {
                toggle.addClass("disabled");
            }
            object.css({"display": "none"});
        }
    };

    $(":checkbox.toggle").each(
        transform(
            function (object) {
                return object.is(":checked");
            }, function (object) {
                return object.prop('checked', true);
            }, function (object) {
                return object.prop('checked', false);
            }, function () {
                return "On";
            }, function () {
                return "Off";
            }
        )
    );

    $("select.toggle").each(
        transform(
            function (object) {
                return object[0].selectedIndex == 2;
            }, function (object) {
                return object[0].selectedIndex = 2;
            }, function (object) {
                return object[0].selectedIndex = 1;
            }, function (object) {
                return object.find('option').eq(2).text();
            }, function (object) {
                return object.find('option').eq(1).text();
            }
        )
    );
})
;
