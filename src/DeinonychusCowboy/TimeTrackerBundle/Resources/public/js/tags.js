$(function () {
    // colors for tag checkboxes
    $('.tagselector').find('input').each(function () {
        $('label[for="' + $(this).attr("id") + '"]').addClass($(this).val());
    });
    // hide tags in filter that aren't on the page
    $(".tag-filters .tag").each(function () {
        if ($("." + $(this).attr("index")).length == 1) {
            $(this).hide();
        }
    });
    // hide empty tag-groups in filter
    $(".tag-filters .tag-group").each(function () {
        if ($(this).find(".tag:visible").length == 0) {
            $(this).hide();
        }
    });
});
