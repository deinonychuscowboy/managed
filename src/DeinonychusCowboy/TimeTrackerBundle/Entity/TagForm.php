<?php


namespace DeinonychusCowboy\TimeTrackerBundle\Entity;

use DeinonychusCowboy\TimeTrackerBundle\Lib\DataManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TagForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		$taggroups = DataManager::getAllTagGroups();
		$choices   = array();
		foreach($taggroups as $group)
		{
			$choices[$group->getId()] = $group;
		}
		$builder->add("name")->add("desc","textarea",array("required" => false))->add(
				"group",
				"entity",
				array(
					"class"       => "TimeTrackerBundle:TagGroup",
					"property"    => "name",
					"required"    => false,
					"empty_value" => "None",
					"choices"     => $choices
				)
			)->add("save","submit");
	}

	public function getName()
	{
		return "tag";
	}
}
