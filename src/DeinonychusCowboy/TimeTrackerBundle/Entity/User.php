<?php


namespace DeinonychusCowboy\TimeTrackerBundle\Entity;

class User
{
	private $timezone;
	private $shortDateFormat;
	private $longDateFormat;
	private $dateSpanFormat;
	private $timeFormat;
	private $timeSpanFormat;
	private $theme;
	private $dayLength;

	public function __construct()
	{
		$this->timezone        = "America/New_York";
		$this->shortDateFormat = "m/d";
		$this->longDateFormat  = "m/d/y";
		$this->dateSpanFormat  = "%d";
		$this->timeFormat      = "h:i";
		$this->timeSpanFormat  = "%H:%I";
		$this->theme           = "cobalt-slate";
		$this->dayLength       = 8;
	}

	/**
	 * @return mixed
	 */
	public function getTimezone()
	{
		return $this->timezone;
	}

	/**
	 * @param mixed $timezone
	 */
	public function setTimezone($timezone)
	{
		$this->timezone = $timezone;
	}

	/**
	 * @return mixed
	 */
	public function getShortDateFormat()
	{
		return $this->shortDateFormat;
	}

	/**
	 * @param mixed $shortDateFormat
	 */
	public function setShortDateFormat($shortDateFormat)
	{
		$this->shortDateFormat = $shortDateFormat;
	}

	/**
	 * @return mixed
	 */
	public function getLongDateFormat()
	{
		return $this->longDateFormat;
	}

	/**
	 * @param mixed $longDateFormat
	 */
	public function setLongDateFormat($longDateFormat)
	{
		$this->longDateFormat = $longDateFormat;
	}

	/**
	 * @return string
	 */
	public function getDateSpanFormat()
	{
		return $this->dateSpanFormat;
	}

	/**
	 * @param string $dateSpanFormat
	 */
	public function setDateSpanFormat($dateSpanFormat)
	{
		$this->dateSpanFormat = $dateSpanFormat;
	}



	/**
	 * @return mixed
	 */
	public function getTimeFormat()
	{
		return $this->timeFormat;
	}

	/**
	 * @param mixed $timeFormat
	 */
	public function setTimeFormat($timeFormat)
	{
		$this->timeFormat = $timeFormat;
	}

	/**
	 * @return mixed
	 */
	public function getTheme()
	{
		return $this->theme;
	}

	/**
	 * @param mixed $theme
	 */
	public function setTheme($theme)
	{
		$this->theme = $theme;
	}

	/**
	 * @return string
	 */
	public function getTimeSpanFormat()
	{
		return $this->timeSpanFormat;
	}

	/**
	 * @param string $timeSpanFormat
	 */
	public function setTimeSpanFormat($timeSpanFormat)
	{
		$this->timeSpanFormat = $timeSpanFormat;
	}

	/**
	 * @return int
	 */
	public function getDayLength()
	{
		return $this->dayLength;
	}

	/**
	 * @param int $dayLength
	 */
	public function setDayLength($dayLength)
	{
		$this->dayLength = $dayLength;
	}
}
