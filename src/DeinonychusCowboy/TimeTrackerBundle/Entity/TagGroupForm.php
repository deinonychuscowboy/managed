<?php


namespace DeinonychusCowboy\TimeTrackerBundle\Entity;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TagGroupForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		$builder->add("name")->add("desc","textarea",array("required" => false))->add("save","submit");
	}

	public function getName()
	{
		return "taggroup";
	}
}
