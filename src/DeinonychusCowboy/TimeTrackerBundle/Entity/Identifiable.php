<?php

namespace DeinonychusCowboy\TimeTrackerBundle\Entity;

interface Identifiable
{
	public function getId();
}
