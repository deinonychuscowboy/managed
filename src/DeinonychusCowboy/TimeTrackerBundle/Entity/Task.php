<?php

namespace DeinonychusCowboy\TimeTrackerBundle\Entity;

use DeinonychusCowboy\TimeTrackerBundle\Controller\DefaultController;
use DeinonychusCowboy\TimeTrackerBundle\Lib\DataManager;
use DeinonychusCowboy\TimeTrackerBundle\Lib\Helper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\PersistentCollection;

/**
 * @Entity
 * @Table(name="Tasks")
 */
class Task implements Identifiable
{
	/**
	 * @var string
	 * @Column(type="string")
	 * @Id
	 */
	private $id;
	/**
	 * @var PersistentCollection
	 * @OneToMany(targetEntity="Interval",mappedBy="task")
	 */
	private $intervals;
	/**
	 * @var int
	 * @Column(type="integer",nullable=true)
	 */
	private $closed;
	/**
	 * @var string
	 * @Column(type="string")
	 */
	private $name;
	/**
	 * @var string
	 * @Column(type="string",nullable=true)
	 */
	private $desc;
	/**
	 * @var PersistentCollection
	 * @ManyToMany(targetEntity="Tag",inversedBy="tasks")
	 * @JoinTable(name="RelTagsTasks")
	 */
	private $tags;

	private function __construct()
	{
	}

	public static function create($name = null,$desc = null)
	{
		$instance       = new self();
		$id             = "T" . Helper::generateId();
		$instance->id   = $id;
		$instance->name = $name
			?: $id;
		$instance->desc = $desc;

		$instance->tags      = new ArrayCollection();
		$instance->intervals = new ArrayCollection();
		$instance->closed    = null;
		DataManager::store($instance);
		DataManager::serialize($id);
		DataManager::commit();
		DataManager::flush($instance);
		unset($instance);

		return DataManager::findById($id);
	}

	public function start()
	{
		if($this->isRunning())
		{
			throw new \Exception("Attempt to start a task that is already running.");
		}
		if($this->closed)
		{
			throw new \Exception("Attempt to start a task that has been closed.");
		}
		$interval = Interval::create();
		$this->intervals->add($interval);
		$interval->setTask($this);
	}

	public function close($note)
	{
		if($this->isRunning())
		{
			throw new \Exception("Attempt to close a task that is running.");
		}
		$this->desc .= "\n\n" . $note;
		$this->closed = time();
	}

	public function addTag($id)
	{
		$tag = DataManager::findById($id);
		$this->tags->add($tag);
		$tag->addObjectRef($this);
	}

	public function removeTag($id)
	{
		$tag = DataManager::findById($id);
		$tag->removeObjectRef($this->getId());
		$this->tags->removeElement($tag);
	}

	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getDesc()
	{
		return $this->desc;
	}

	/**
	 * @return array
	 */
	public function getIntervals()
	{
		return $this->intervals;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return boolean
	 */
	public function isRunning()
	{
		return $this->getRunningInterval() !== null;
	}

	public function getRunningInterval()
	{
		foreach($this->intervals->getValues() as $interval)
		{
			if($interval->getStop() === null)
			{
				return $interval;
			}
		}

		return null;
	}

	public function getLastInterval()
	{
		$start = date_create("@0");
		$which = null;
		foreach($this->intervals->getValues() as $interval)
		{
			if($interval->getStart() > $start)
			{
				$start = $interval->getStart();
				$which = $interval;
			}
		}

		return $which;
	}

	public function getTime()
	{
		$total   = $this->getUnixtime();
		$seconds = $total % 60;
		$total -= $seconds;
		$total /= 60;
		$minutes = $total % 60;
		$total -= $minutes;
		$total /= 60;
		$hours = $total % DefaultController::getCurrentUser()->getDayLength();
		$total -= $hours;
		$total /= DefaultController::getCurrentUser()->getDayLength();
		$workdays = $total;

		return new \DateInterval("P" . $workdays . "DT" . $hours . "H" . $minutes . "M" . $seconds . "S");
	}

	public function getUnixtime()
	{
		$total = 0;
		foreach($this->intervals->getValues() as $interval)
		{
			$total += $interval->getSeconds();
		}

		return $total;
	}

	/**
	 * @return boolean
	 */
	public function isClosed()
	{
		return $this->closed !== null;
	}

	public function getCloseUnixtime()
	{
		return $this->closed;
	}

	public function getTags()
	{
		$arr = $this->tags->getValues();
		$ret = array();
		foreach($arr as $obj)
		{
			$ret[$obj->getId()] = $obj;
		}

		return $ret;
	}

	/**
	 * @return array
	 */
	public function getTagIds()
	{
		$arr = array();
		foreach($this->getTags() as $tag)
		{
			$arr[] = $tag->getId();
		}

		return $arr;
	}

	public function setTagIds($ids)
	{
		$tags = $this->getTags();
		foreach($ids as $id)
		{
			if(!array_key_exists($id,$tags))
			{
				$this->addTag($id);
			}
		}
		foreach(array_keys($tags) as $id)
		{
			if(!in_array($id,$ids))
			{
				$this->removeTag($id);
			}
		}
	}

	public function getStatus()
	{
		if($this->isRunning())
		{
			return "RUNNING";
		}
		elseif($this->closed !== null)
		{
			return "CLOSED";
		}
		elseif(count($this->intervals) === 0)
		{
			return "TODO";
		}
		else
		{
			return "OPEN";
		}
		// TODO if has dependencies, blocked
	}

	/**
	 * @param boolean $closed
	 */
	public function setClosed($closed)
	{
		$this->closed = $closed
			? time()
			: null;
	}

	/**
	 * @param mixed $desc
	 */
	public function setDesc($desc)
	{
		$this->desc = $desc;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}
}
