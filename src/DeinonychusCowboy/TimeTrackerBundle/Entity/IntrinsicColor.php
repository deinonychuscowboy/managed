<?php


namespace DeinonychusCowboy\TimeTrackerBundle\Entity;

use DeinonychusCowboy\TimeTrackerBundle\Lib\Helper;

abstract class IntrinsicColor
{
	private   $hash;
	protected $colorSeed;

	protected abstract function setColorSeed();

	private function getColorInternal()
	{
		if($this->colorSeed === null)
		{
			$this->setColorSeed();
			if($this->colorSeed === null)
			{
				throw new \Exception("Seed not Set!");
			}
		}
		if($this->hash === null)
		{
			$this->hash = Helper::generateHash($this->colorSeed);
		}
	}

	public function getMainColor()
	{
		$this->getColorInternal();

		return Helper::getMainColor($this->hash);
	}

	public function getLightColor()
	{
		$this->getColorInternal();

		return Helper::getLightColor($this->hash);
	}

	public function getPastelColor()
	{
		$this->getColorInternal();

		return Helper::getPastelColor($this->hash);
	}
}
