<?php

namespace DeinonychusCowboy\TimeTrackerBundle\Entity;

use DateInterval;
use DateTimeZone;
use DeinonychusCowboy\TimeTrackerBundle\Controller\DefaultController;
use DeinonychusCowboy\TimeTrackerBundle\Lib\DataManager;
use DeinonychusCowboy\TimeTrackerBundle\Lib\Helper;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\PersistentCollection;

/**
 * @Entity
 * @Table(name="Intervals")
 */
class Interval implements Identifiable
{
	/**
	 * @var string
	 * @Column(type="string")
	 * @Id
	 */
	private $id;
	/**
	 * @var int
	 * @Column(type="integer")
	 */
	private $start;
	/**
	 * @var int
	 * @Column(type="integer",nullable=true)
	 */
	private $stop;
	/**
	 * @var string
	 * @Column(type="string",nullable=true)
	 */
	private $note;
	/**
	 * @var Task
	 * @ManyToOne(targetEntity="Task",inversedBy="intervals")
	 * @JoinColumn(name="task_id", referencedColumnName="id")
	 */
	private $task;

	private function __construct()
	{
	}

	public static function create()
	{
		$instance        = new self();
		$id              = "I" . Helper::generateId();
		$instance->id    = $id;
		$instance->start = time();
		$instance->stop  = null;
		DataManager::store($instance);
		DataManager::serialize($id);
		DataManager::commit();
		DataManager::flush($instance);
		unset($instance);

		return DataManager::findById($id);
	}

	public function setStop($datetime)
	{
		$this->stop = (int)date_format($datetime,"U");
	}

	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return mixed
	 */
	public function getNote()
	{
		return $this->note;
	}

	/**
	 * @return \DateTime
	 */
	public function getStart()
	{
		return date_create("@" . $this->start);
	}

	/**
	 * @param int $start
	 */
	public function setStart($datetime)
	{
		$this->start = (int)date_format($datetime,"U");
	}

	/**
	 * @return \DateTime
	 */
	public function getStop()
	{
		return $this->stop === null
			? null
			: date_create("@" . $this->stop);
	}

	public function getTime()
	{
		$total   = $this->getSeconds();
		$seconds = $total % 60;
		$total -= $seconds;
		$total /= 60;
		$minutes = $total % 60;
		$total -= $minutes;
		$total /= 60;
		$hours = $total % DefaultController::getCurrentUser()->getDayLength();
		$total -= $hours;
		$total /= DefaultController::getCurrentUser()->getDayLength();
		$workdays = $total;

		return new \DateInterval("P" . $workdays . "DT" . $hours . "H" . $minutes . "M" . $seconds . "S");
	}

	public function getSeconds()
	{
		$stop = $this->stop
			?: time();

		return $stop - $this->start;
	}

	/**
	 * @param string $note
	 */
	public function setNote($note)
	{
		$this->note = $note;
	}

	/**
	 * @param Task $task
	 */
	public function setTask($task)
	{
		if($this->task === null)
		{
			$this->task = $task;
		}
	}

	/**
	 * @return Task
	 */
	public function getTask()
	{
		return $this->task;
	}
}
