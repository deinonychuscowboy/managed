<?php

namespace DeinonychusCowboy\TimeTrackerBundle\Entity;

use DeinonychusCowboy\TimeTrackerBundle\Lib\DataManager;
use DeinonychusCowboy\TimeTrackerBundle\Lib\Helper;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\PersistentCollection;

/**
 * @Entity
 * @Table(name="Tags")
 */
class Tag extends IntrinsicColor implements Identifiable
{

	/**
	 * @var string
	 * @Column(type="string")
	 * @Id
	 */
	private $id;
	/**
	 * @var string
	 * @Column(type="string")
	 */
	private $name;
	/**
	 * @var PersistentCollection
	 * @ManyToMany(targetEntity="Task",mappedBy="tags")
	 */
	private $tasks;

	/**
	 * @var string
	 * @Column(type="string",nullable=true)
	 */
	private $desc;

	/**
	 * @var TagGroup
	 * @ManyToOne(targetEntity="TagGroup",inversedBy="tags")
	 * @JoinColumn(name="group_id", referencedColumnName="id")
	 */
	private $group;

	public function getId()
	{
		return $this->id;
	}

	private function __construct()
	{
	}

	public static function create($name = null,$desc = null)
	{
		$instance        = new self();
		$id              = "G" . Helper::generateId();
		$instance->name  = $name
			?: $id;
		$instance->id    = $id;
		$instance->tasks = array();
		$instance->desc  = "";
		DataManager::store($instance);
		DataManager::serialize($id);
		DataManager::commit();
		DataManager::flush($instance);
		unset($instance);

		return DataManager::findById($id);
	}

	public function addObjectRef(Identifiable $obj)
	{
		$this->tasks[$obj->getId()] = $obj;
	}

	public function removeObjectRef($id)
	{
		unset($this->tasks[$id]);
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return array
	 */
	public function getTasks()
	{
		return $this->tasks;
	}

	protected function setColorSeed()
	{
		$this->colorSeed = $this->id;
	}

	/**
	 * @return string
	 */
	public function getDesc()
	{
		return $this->desc;
	}

	/**
	 * @param string $desc
	 */
	public function setDesc($desc)
	{
		$this->desc = $desc;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return TagGroup
	 */
	public function getGroup()
	{
		return $this->group;
	}

	/**
	 * @param int $group
	 */
	public function setGroup($group)
	{
		$this->group = is_integer($group)
			? DataManager::findById($group)
			: $group;
	}
}
