<?php


namespace DeinonychusCowboy\TimeTrackerBundle\Entity;

use DeinonychusCowboy\TimeTrackerBundle\Lib\DataManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Form\FormBuilderInterface;

class TaskForm extends AbstractType
{
	private $closed;
	private $filter;

	public function __construct($filter)
	{
		$this->closed = false;
		$this->filter = $filter;
	}

	public function setClosed($bool)
	{
		$this->closed = $bool;
	}

	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		$builder->add("name","text",array("disabled" => $this->closed))->add(
			"closed",
			"choice",
			array(
				"required"    => false,
				"disabled"    => $this->closed,
				"choice_list" => new ChoiceList(array(false,true),array("Open","Closed")),
				"attr"        => array("class" => "toggle")
			)
		)->add(
			"desc",
			"textarea",
			array("required" => false,"disabled" => $this->closed)
		);
		$groups  = DataManager::getAllTagGroups();
		$choices = array();
		foreach($groups as $group)
		{
			foreach($group->getTags() as $tag)
			{
				$choices[$tag->getId()] = $tag->getName();
			}
		}
		$tags = DataManager::getAllTags();
		foreach($tags as $tag)
		{
			if($tag->getGroup() == null)
			{
				$choices[$tag->getId()] = $tag->getName();
			}
		}

		$args = array(
			"required" => false,
			"choices"  => $choices,
			"multiple" => true,
			"expanded" => true,
			"label"    => "Tags",
			"disabled" => $this->closed,
			"attr"     => array("class" => "tagselector"),
		);
		if($this->filter != null)
		{
			$args["data"] = array($this->filter);
		}
		$builder->add(
			"tagIds",
			"choice",
			$args
		);
		if(!$this->closed)
		{
			$builder->add("save","submit");
		}
	}

	public function getName()
	{
		return "task";
	}
}
