<?php


namespace DeinonychusCowboy\TimeTrackerBundle\Entity;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class IntervalForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder,array $options)
	{
		$builder->add("start","datetime",array("with_seconds" => true,"model_timezone" => "UTC"))->add(
				"stop",
				"datetime",
				array("with_seconds" => true,"model_timezone" => "UTC")
			)->add("note","textarea",array("required" => false))->add("save","submit");
	}

	public function getName()
	{
		return "interval";
	}
}
