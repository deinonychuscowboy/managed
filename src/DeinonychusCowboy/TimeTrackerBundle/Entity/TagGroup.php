<?php

namespace DeinonychusCowboy\TimeTrackerBundle\Entity;

use DeinonychusCowboy\TimeTrackerBundle\Lib\DataManager;
use DeinonychusCowboy\TimeTrackerBundle\Lib\Helper;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\PersistentCollection;

/**
 * @Entity
 * @Table(name="TagGroups")
 */
class TagGroup extends IntrinsicColor implements Identifiable
{

	/**
	 * @var string
	 * @Column(type="string")
	 * @Id
	 */
	private $id;
	/**
	 * @var string
	 * @Column(type="string")
	 */
	private $name;
	/**
	 * @var PersistentCollection
	 * @OneToMany(targetEntity="Tag",mappedBy="group")
	 */
	private $tags;

	/**
	 * @var string
	 * @Column(type="string",nullable=true)
	 */
	private $desc;

	public function getId()
	{
		return $this->id;
	}

	private function __construct()
	{
	}

	public static function create($name = null,$desc = null)
	{
		$instance       = new self();
		$id             = "R" . Helper::generateId();
		$instance->name = $name
			?: $id;
		$instance->id   = $id;
		$instance->tags = array();
		$instance->desc = "";
		DataManager::store($instance);
		DataManager::serialize($id);
		DataManager::commit();
		DataManager::flush($instance);
		unset($instance);

		return DataManager::findById($id);
	}

	public function addObjectRef(Identifiable $obj)
	{
		$this->tags[$obj->getId()] = $obj;
	}

	public function removeObjectRef($id)
	{
		unset($this->tags[$id]);
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return array
	 */
	public function getTags()
	{
		return $this->tags;
	}

	protected function setColorSeed()
	{
		$this->colorSeed = $this->id;
	}

	/**
	 * @return string
	 */
	public function getDesc()
	{
		return $this->desc;
	}

	/**
	 * @param string $desc
	 */
	public function setDesc($desc)
	{
		$this->desc = $desc;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}
}
