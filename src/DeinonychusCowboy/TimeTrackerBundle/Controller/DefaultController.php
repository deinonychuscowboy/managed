<?php

namespace DeinonychusCowboy\TimeTrackerBundle\Controller;

use DeinonychusCowboy\TimeTrackerBundle\Entity\Interval;
use DeinonychusCowboy\TimeTrackerBundle\Entity\IntervalForm;
use DeinonychusCowboy\TimeTrackerBundle\Entity\Tag;
use DeinonychusCowboy\TimeTrackerBundle\Entity\TagForm;
use DeinonychusCowboy\TimeTrackerBundle\Entity\TagGroup;
use DeinonychusCowboy\TimeTrackerBundle\Entity\TagGroupForm;
use DeinonychusCowboy\TimeTrackerBundle\Entity\Task;
use DeinonychusCowboy\TimeTrackerBundle\Entity\User;
use DeinonychusCowboy\TimeTrackerBundle\Lib\DataManager;
use DeinonychusCowboy\TimeTrackerBundle\Entity\TaskForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
	private static $user;

	/**
	 * @return mixed
	 */
	public static function getCurrentUser()
	{
		return self::$user;
	}

	public function init($doctrine)
	{
		self::$user = new User();
		date_default_timezone_set(self::getCurrentUser()->getTimezone());
		DataManager::init($doctrine);
	}

	public function shutdown()
	{
		DataManager::serializeAll();
		DataManager::commit();
	}

	/**
	 * @Route("/")
	 * @Template()
	 */
	public function indexAction(Request $request)
	{
		return $this->redirect("tasklist");
	}

	/**
	 * @Route("/tasklist")
	 * @Template()
	 */
	public function tasklistAction(Request $request)
	{
		$this->init($this->getDoctrine());
		$limit = 30;
		$tasks = array();
		if($request->query->has("all"))
		{
			$tasks = DataManager::getAllTasks($limit);
		}
		else
		{
			$tasks = DataManager::getAllOpenTasks($limit);
		}

		$filter = $request->query->getAlnum("filter");
		if($request->query->has("filter") && !empty($filter))
		{
			$tasks = DataManager::filterWithTag($tasks,$filter);
		}

		return $this->finalize(
			array(
				"user"       => self::getCurrentUser(),
				"filter"     => $request->query->get("filter"),
				"tasks"      => $tasks,
				"tags"       => DataManager::getAllTags(),
				"tag_groups" => DataManager::getAllTagGroups()
			)
		);
	}

	/**
	 * @Route("/raw")
	 * @Template()
	 */
	public function rawlistAction(Request $request)
	{
		$this->init($this->getDoctrine());
		$tasks = DataManager::getAllOpenTasks();

		return $this->finalize(
			array(
				"user"  => self::getCurrentUser(),
				"tasks" => $tasks,
				"tags"  => DataManager::getAllTags(),
			)
		);
	}

	/**
	 * @Route("/taglist")
	 * @Template()
	 */
	public function taglistAction(Request $request)
	{
		$this->init($this->getDoctrine());
		$tags      = DataManager::getAllTags();
		$taggroups = DataManager::getAllTagGroups();

		// TODO show usage count
		return $this->finalize(
			array(
				"user"       => self::getCurrentUser(),
				"tags"       => $tags,
				"tag_groups" => $taggroups,
			)
		);
	}

	/**
	 * @Route("/intervallist")
	 * @Template()
	 */
	public function intervallistAction(Request $request)
	{
		$this->init($this->getDoctrine());
		$intervals = DataManager::getAllIntervals();

		$filter = $request->query->getAlnum("filter");
		if($request->query->has("filter") && !empty($filter))
		{
			$intervals = DataManager::filterWithTag(
				$intervals,
				$request->query->getAlnum("filter"),
				function ($interval)
				{
					return $interval->getTask();
				}
			);
		}

		// TODO show usage count
		return $this->finalize(
			array(
				"user"       => self::getCurrentUser(),
				"tags"       => DataManager::getAllTags(),
				"tag_groups" => DataManager::getAllTagGroups(),
				"intervals"  => $intervals,
				"filter"     => $request->query->getAlnum("filter"),
			)
		);
	}

	private function formAction(Request $request,$callableNewForm,$callableNewObject)
	{
		$form = null;
		$new  = true;
		$obj  = null;
		if($request->query->has("id"))
		{
			$obj  = DataManager::findById($request->query->getAlnum("id"));
			$form = $this->createForm($callableNewForm($obj,0),$obj);
			$new  = false;
		}
		else if($request->getMethod() === "POST")
		{
			$obj  = $callableNewObject();
			$form = $this->createForm($callableNewForm($obj,1),$obj);
		}
		else
		{
			$form = $this->createForm($callableNewForm(null,2),null);
		}

		if($request->getMethod() === "POST")
		{
			$form->handleRequest($request);

			if($form->isValid())
			{
				return $this->finalize(
					null,
					"tasklist" . ($request->query->has("filter")
						? "?filter=" . $request->query->getAlnum("filter")
						: "")
				);
			}
		}

		return array(
			"user"   => self::getCurrentUser(),
			"id"     => $new
				? null
				: $request->query->getAlnum("id"),
			"filter" => $request->query->getAlnum("filter"),
			"form"   => $form->createView(),
			"tags"   => DataManager::getAllTags(),
			"new"    => $new,
			"obj"    => $obj,
		);
	}

	/**
	 * @Route("/task")
	 * @Template()
	 */
	public function taskAction(Request $request)
	{
		$this->init($this->getDoctrine());

		return $this->finalize(
			$this->formAction(
				$request,
				function ($task,$state) use ($request)
				{
					$form = new TaskForm(
						$state == 2
							? $request->query->getAlnum('filter')
							: null
					);
					$form->setClosed(
						$task !== null
							? $task->isClosed()
							: false
					);

					return $form;
				},
				function ()
				{
					return Task::create();
				}
			)
		);
	}

	/**
	 * @Route("/tag")
	 * @Template()
	 */
	public function tagAction(Request $request)
	{
		$this->init($this->getDoctrine());

		return $this->finalize(
			$this->formAction(
				$request,
				function ($tag)
				{
					return new TagForm();
				},
				function ()
				{
					return Tag::create();
				}
			)
		);
	}

	/**
	 * @Route("/taggroup")
	 * @Template()
	 */
	public function tagGroupAction(Request $request)
	{
		$this->init($this->getDoctrine());

		return $this->finalize(
			$this->formAction(
				$request,
				function ($taggroup)
				{
					return new TagGroupForm();
				},
				function ()
				{
					return TagGroup::create();
				}
			)
		);
	}

	/**
	 * @Route("/interval")
	 * @Template()
	 */
	public function intervalAction(Request $request)
	{
		$this->init($this->getDoctrine());
		if($request->query->has("id"))
		{
			$interval = DataManager::findById($request->query->getAlnum("id"));
			if($interval->getStop() === null)
			{
				$interval->setStop(date_create());
			}
		}

		return $this->finalize(
			$this->formAction(
				$request,
				function ($interval)
				{
					return new IntervalForm();
				},
				function ()
				{
					return null; // created from Task
				}
			)
		);
	}

	private function finalize($return,$redirect = null)
	{
		$this->shutdown();
		if($redirect !== null)
		{
			return $this->redirect($redirect);
		}

		return $return;
	}

	/**
	 * @Route("/taskstart")
	 * @Template()
	 */
	public function taskStartAction(Request $request)
	{
		$this->init($this->getDoctrine());
		if($request->query->has("id"))
		{
			$task = DataManager::findById($request->query->getAlnum("id"));
			$task->start();
		}

		return $this->finalize(
			array("user" => self::getCurrentUser(),"filter" => $request->query->getAlnum("filter")),
			"tasklist" . ($request->query->has("filter")
				? "?filter=" . $request->query->getAlnum("filter")
				: "")
		);
	}

	/**
	 * @Route("/taskstop")
	 * @Template()
	 */
	public function taskStopAction(Request $request)
	{
		$this->init($this->getDoctrine());
		if($request->query->has("id"))
		{
			$task = DataManager::findById($request->query->getAlnum("id"));

			return $this->finalize(
				null,
				"interval?id=" . $task->getRunningInterval()->getId() . ($request->query->has("filter")
					? "&filter=" . $request->query->getAlnum("filter")
					: "")
			);
		}

		return $this->finalize(
			array("user" => self::getCurrentUser(),"filter" => $request->query->getAlnum("filter")),
			"tasklist" . ($request->query->has("filter")
				? "?filter=" . $request->query->getAlnum("filter")
				: "")
		);
	}

	/**
	 * @Route("/report")
	 * @Template()
	 */
	public function reportAction(Request $request)
	{
		$this->init($this->getDoctrine());
		$tags        = DataManager::getAllTags();
		$closedTasks = DataManager::getAllClosedTasks();
		$period      = time();
		// find earliest close date
		foreach($closedTasks as $task)
		{
			$period = $task->getCloseUnixtime() < $period
				? $task->getCloseUnixtime()
				: $period;
		}
		// sort tasks by date
		uasort(
			$closedTasks,
			function ($a,$b)
			{
				return $a->getCloseUnixtime() - $b->getCloseUnixtime();
			}
		);
		$countClosedTasksPayload = array();
		// calculate amounts for each tag
		foreach($tags as $tag)
		{
			$tagTasks        = DataManager::filterWithTag($closedTasks,$tag->getId());
			$tagColor        = $this->rgbarrToHex($tag->getPastelColor());
			$pointColor      = $this->rgbarrToHex($tag->getLightColor());
			$countDataPoints = array();
			$timeDataPoints  = array();
			// iterate to get totals by day
			for($i = $period;$i <= strtotime("tomorrow");$i += 86400)
			{
				$rem       = array();
				$totalTime = 0;
				foreach($tagTasks as $task)
				{
					if($task->getCloseUnixtime() > $i)
					{
						break;
					}
					else
					{
						$rem[] = $task;
						$totalTime += $task->getUnixtime();
					}
				}
				$countDataPoints[] = array("x" => $i * 1000,"y" => count($rem),"color" => $pointColor);
				$timeDataPoints[]  = array("x" => $i * 1000,"y" => $totalTime,"color" => $pointColor);
				foreach($rem as $task)
				{
					array_shift($tagTasks);
				}
			}

			$countClosedTasksPayload[] = array(
				"type"  => "area",
				"name"  => $tag->getName(),
				"data"  => $countDataPoints,
				"color" => $tagColor,
			);
			$timeClosedTasksPayload[]  = array(
				"type"  => "area",
				"name"  => $tag->getName(),
				"data"  => $timeDataPoints,
				"color" => $tagColor,
			);
		}

		return $this->finalize(
			array(
				"user"                    => self::getCurrentUser(),
				"closedInPeriod"          => DataManager::filterByCloseDate(
					$closedTasks,
					$period,
					time()
				),
				"tags"                    => $tags,
				"countClosedTasksPayload" => $countClosedTasksPayload,
				"timeClosedTasksPayload"  => $timeClosedTasksPayload
			)
		);
	}

	private static function rgbarrToHex($rgbarr)
	{
		return "#" . dechex($rgbarr[0]) . dechex($rgbarr[1]) . dechex($rgbarr[2]);
	}
}
