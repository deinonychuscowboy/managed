Managed is a simple time tracker implemented in PHP using Symfony. Managed will no longer be maintained; Derecho is being developed to replace it for my purposes.

I started writing Managed because every time tracking application out there is either A. far too complicated, B. not platform-agnostic, or C. not task-focused. Managed is meant for very loose time management, with no complex project features and only a few main views. Every data object that isn't a task should stay out of your way.

Features
========
Managed provides no-nonsense task management:
 - Basic data unit is a Task, which includes a Name and Description.
 - Tasks can be tagged with multiple tags, each one with a description.
 - Tags can be grouped to aid organization. *[0.6]*
 - A task can be started and stopped, and time spent on a task is reported.
 - Each interval of time spent on a task can have its own note to report what was done.
 - Tasks can be closed when they are finished.
 - You can display only tasks with a particular tag by clicking on that tag at the top of the tasklist.
 - To view closed tasks, click the button at the top of the tasklist. *[0.5]*
 - Generate reports on your task history. *[0.4]*
   - Tag Clearance: How much closing activity a particular tag has seen.
   - Time Clearance: How much time has been devoted to finished tasks.
   
Installation Instructions
=========================

1. [Get composer](https://getcomposer.org/download/).
2. Run `composer install`.
3  Make a directory named 'lib' in the Symfony folder. 
4. Run `php app/console doctrine:database:create`.
5. Run `php app/console doctrine:schema:update --force`.
6. Point your web server to the web directory using whatever host address you like.
7. Fire up your browser, add some tasks and tags, and get to work.

Installation Troubleshooting
============================

 - Make sure your webserver is properly set up to handle Symfony. An .htaccess file is included.
 - Make sure your webserver has the correct permissions to access and execute Symfony files.
 - Make sure your webserver's virtual host is pointed to the managed/web folder.
 - Make sure you followed the instructions closely.
 - Make sure you report issues!

Update Notes
============

If you have a version of Managed prior to the introduction of tag groups (version <= 0.5), be sure to run
`php app/console doctrine:schema:update --force`
when updating to the latest version.
